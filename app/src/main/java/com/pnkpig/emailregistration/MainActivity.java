package com.pnkpig.emailregistration;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper dbHelper;
    List<EmailInformation> emails;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        Button btn = (Button)findViewById(R.id.buttonRegistrationForm);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/plsenpai.ttf");
        btn.setTypeface(tf);
        btn = (Button)findViewById(R.id.buttonViewAllEmails);
        btn.setTypeface(tf);
        btn = (Button)findViewById(R.id.buttonSaveAllEmails);
        btn.setTypeface(tf);
        btn = (Button)findViewById(R.id.buttonDeleteAllEmails);
        btn.setTypeface(tf);

        dbHelper = new DatabaseHelper(getApplicationContext());
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        emails = dbHelper.getAllEmails();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnOnClick(View view) {
        Intent i;
        switch(view.getId()){
            case R.id.buttonRegistrationForm:
                i = new Intent(this, RegistrationActivity.class);
                startActivity(i);
                return;
            case R.id.buttonViewAllEmails:
                i = new Intent(this, ViewEmailsActivity.class);
                startActivity(i);
                return;
            case R.id.buttonSaveAllEmails:
                emails = dbHelper.getAllEmails();
                saveRecipes(emails);
                return;
            case R.id.buttonDeleteAllEmails:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Are you sure you want to delete ALL emails?")
                        .setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

                return;

        }
    }

    public void saveRecipes(List<EmailInformation> listEmails){
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOCUMENTS), "EmailList");
        if (!file.mkdirs()) {
            Log.e("Error", "Directory not created");
        }
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(file.getAbsolutePath() +".csv"));

            List<String[]> data = new ArrayList<String[]>();
            for(int i = 0; i < listEmails.size(); ++i) {
                data.add(new String[]
                        { listEmails.get(i).name_,
                                listEmails.get(i).email_});
            }
            writer.writeAll(data);
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        Toast.makeText(this, "Saved file 'EmailList.csv' to Documents!", Toast.LENGTH_LONG).show();
    }

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Are you REALLY sure? This cannot be undone!")
                            .setPositiveButton("Yes", dialogClickListener2)
                            .setNegativeButton("No", dialogClickListener2).show();

                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogClickListener2 = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Yes button clicked
                    dbHelper.deleteAllEmails();
                    tellUser("Deleted all email addresses");
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };
    public void tellUser(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }
}
