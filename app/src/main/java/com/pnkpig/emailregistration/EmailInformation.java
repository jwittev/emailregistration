package com.pnkpig.emailregistration;

/**
 * Created by Jenna on 7/29/15.
 */
public class EmailInformation {
    public String name_, email_;

    public EmailInformation(String n, String e) {
        name_ = n; email_ = e;
    }
}
