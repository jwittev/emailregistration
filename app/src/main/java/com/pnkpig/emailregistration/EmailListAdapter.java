package com.pnkpig.emailregistration;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Jenna on 7/29/15.
*/

public class EmailListAdapter extends BaseAdapter {

    private Context mContext;
    public List<EmailInformation> mNames;

    public EmailListAdapter(Context c, List<EmailInformation> names){
        mContext = c;
        mNames = names;
    }

    @Override
    public int getCount() {
        return mNames.size();
    }

    @Override
    public Object getItem(int position) {
        return mNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout ll = new LinearLayout(mContext);
        ll.setOrientation(LinearLayout.HORIZONTAL);

        TextView tvName = new TextView(mContext);
        tvName.setText(mNames.get(position).name_);
        tvName.setTextSize(20.0f);
        tvName.setPadding(20, 20, 20, 20);

        TextView tvEmail = new TextView(mContext);
        tvEmail.setText(mNames.get(position).email_);
        tvEmail.setTextSize(20.0f);
        tvEmail.setPadding(20, 20, 20, 20);

        if(position == 0) {
            // do stuff for header
            tvName.setBackgroundColor(Color.BLACK);
            tvEmail.setBackgroundColor(Color.BLACK);

            tvName.setTextColor(Color.WHITE);
            tvEmail.setTextColor(Color.WHITE);

            tvName.setTextSize(22.0f);
            tvEmail.setTextSize(22.0f);
        }
        ll.addView(tvName);
        ll.addView(tvEmail);
        return ll;
    }

}