package com.pnkpig.emailregistration;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

public class RegistrationActivity extends AppCompatActivity {

    String name;
    String email;
    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/plsenpai.ttf");

        TextView tv = (TextView)findViewById(R.id.tvName);
        tv.setTypeface(tf);
        tv = (TextView)findViewById(R.id.tvEmail);
        tv.setTypeface(tf);

        Button btn = (Button)findViewById(R.id.buttonSignUp);
        btn.setTypeface(tf);

        EditText et = (EditText)findViewById(R.id.etName);
        et.setTypeface(tf);
        et = (EditText)findViewById(R.id.etEmail);
        et.setTypeface(tf);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_registration, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void buttonClickSignUp(View view) {
        EditText etN = (EditText)findViewById(R.id.etName);
        name = etN.getText().toString();

        EditText etE = (EditText)findViewById(R.id.etEmail);
        email = etE.getText().toString();
        if(email.equals("")){
            Toast.makeText(this, "Please enter an email address", Toast.LENGTH_LONG).show();
            return;
        }
        // Write name and email options to db
        dbHelper = new DatabaseHelper(getApplicationContext());
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(dbHelper.writePersonToDatabase(name, email)){
            String msg = "Added " + name + " to the list!";
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            etN.setText("");
            etE.setText("");
        }

    }
}
