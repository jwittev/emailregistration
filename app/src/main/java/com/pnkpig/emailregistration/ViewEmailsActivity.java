package com.pnkpig.emailregistration;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;

public class ViewEmailsActivity extends AppCompatActivity {

    DatabaseHelper dbHelper;
    List<EmailInformation> listEmails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_emails);

        TextView t1 = (TextView)findViewById(R.id.textView3);
        TextView t2 = (TextView)findViewById(R.id.textView4);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/plsenpai.ttf");
        t1.setTypeface(tf); t2.setTypeface(tf);

        // Write name and email options to db
        dbHelper = new DatabaseHelper(getApplicationContext());
        try {
            dbHelper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        listEmails = dbHelper.getAllEmails();

        // get a reference for the TableLayout
        TableLayout table = (TableLayout)findViewById(R.id.tableEmails);
        // create a new TableRow
        for(int i = 0; i < listEmails.size(); ++i) {
            if(!listEmails.get(i).email_.equals("")) {
                TableRow row = new TableRow(this);
                TextView tName = new TextView(this);
                TextView tEmail = new TextView(this);

                tName.setText(listEmails.get(i).name_);
                tEmail.setText(listEmails.get(i).email_);

                tName.setTextSize(24);
                tEmail.setTextSize(24);

                tName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                tEmail.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                tName.setTypeface(tf);
                tEmail.setTypeface(tf);
                // add the TextView  to the new TableRow
                row.addView(tName);
                row.addView(tEmail);
                row.setPadding(20, 20, 20, 20);
                // add the TableRow to the TableLayout
                table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_emails, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
