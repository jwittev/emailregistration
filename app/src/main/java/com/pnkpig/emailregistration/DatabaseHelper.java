package com.pnkpig.emailregistration;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "dbemails.db";
    private static final String DB_PATH = "/data/data/com.pnkpig.emailregistration/databases/";
    private static final int DB_VERSION = 1;

    //private SQLiteDatabase myDB;
    private Context context;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    private boolean checkDataBase() {
        SQLiteDatabase tempDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            tempDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        } catch (SQLiteException e) {
            Log.e("tle99 - check", e.getMessage());
        }
        if (tempDB != null)
            tempDB.close();
        return tempDB != null ? true : false;
    }

    /**
     * Copy database from source code assets to device
     *
     * @throws IOException
     */
    public void copyDataBase() throws IOException {
        try {
            InputStream myInput = context.getAssets().open(DB_NAME);
            String outputFileName = DB_PATH + DB_NAME;
            OutputStream myOutput = new FileOutputStream(outputFileName);

            byte[] buffer = new byte[1024];
            int length;

            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            myOutput.flush();
            myOutput.close();
            myInput.close();
        } catch (Exception e) {
            Log.e("tle99 - copyDatabase", e.getMessage());
        }

    }

    public boolean writePersonToDatabase(String name, String email) {
        SQLiteDatabase db = this.getWritableDatabase();

        long id = -1;

        try {
            ContentValues values = new ContentValues();
            values.put("name", name);
            values.put("email", email);
            id = db.insert("email_information", null, values);
        } catch (Exception e) {
            Log.e("tle99", e.getMessage());
        }
        db.close();
        if(id == -1)
            return false;
        else
            return true;
    }

    public boolean deleteAllEmails() {
        SQLiteDatabase db = this.getWritableDatabase();

        long id = -1;

        try {
            id = db.delete("email_information", null, null);
        } catch (Exception e) {
            Log.e("tle99", e.getMessage());
        }
        db.close();
        if(id == -1)
            return false;
        else
            return true;
    }

    /**
     * Check if the database doesn't exist on device, create new one
     *
     * @throws IOException
     */
    public void createDataBase() throws IOException {
        boolean dbExist = checkDataBase();

        if (dbExist) {

        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
                //Log.e("tle99 - create", e.getMessage());
            }
        }
    }

    public List<EmailInformation> getAllEmails() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c;
        List<EmailInformation> emails = new ArrayList<EmailInformation>();

        try {
            c = db.rawQuery("SELECT name, email " +
                    "FROM email_information", null);

            if (c == null) return null;
            c.moveToFirst();
            do {
                emails.add(new EmailInformation(c.getString(0), c.getString(1)));
            } while (c.moveToNext());
            c.close();
        } catch (Exception e) {
            Log.e("tle99", e.getMessage());
        }
        db.close();
        return emails;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}